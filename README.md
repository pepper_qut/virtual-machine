# README #
### Download the virtual machine ###

The latest version of the virtual machine can be downloaded [this link](https://cloudstor.aarnet.edu.au/plus/s/yjPlcwcZly18S87)

**changelog:**

- Fixed an issue with eth0 not being found at boot due to it being reserved
- Fixed a naming discrepancy with libboost-python between the virtual machin and the robot

### Previous versions ###
- [Version 1.0.0](https://cloudstor.aarnet.edu.au/plus/s/jzzCnZaFVuwagMZ)

### Accessing the virtual machine ###

The easiest way to access the virtual machine is to use port forwarded between the host and client machines. For instance, we port forward port 2222 on the host to port 22 on the client. We can then access the client using the ssh command:

```
ssh nao@localhost -p 2222
```

**Note:** The login usernamd and password for the virtualbox are both ***nao***

### Building ROS ###
To build ROS, run the following command within the virtual machine:
```
cd ~/my_workspace
./build
```

**Note:** This builds ROS and all of the relevant dependencies, and can take several hours.

Once this is finished, you can deploy the built ROS filesystem to the robot by running the command:
```
pepper_deploy --core
```

### Setting up a Catkin Workspace ###
Once you have built ROS, you can add your own packages by setting up a catkin workspace.

```
source ~/my_workspace/ros_toolchain_install/setup.bash
~/catkin_workspace
catkin_make
```

**Note:** For instructions on how to use catkin, and on how to source ROS workspaces between logins, see the ROS documentation.

### Academic Usage ###
If this software is being used for academic purposes, we ask that you cite our paper: 
```
@ARTICLE{2018arXiv180403288S,
   author = {{Suddrey}, G. and {Jacobson}, A. and {Ward}, B.},
    title = "{Enabling a Pepper Robot to provide Automated and Interactive Tours of a Robotics Laboratory}",
  journal = {ArXiv e-prints},
archivePrefix = "arXiv",
   eprint = {1804.03288},
 primaryClass = "cs.RO",
 keywords = {Computer Science - Robotics},
     year = 2018,
    month = apr,
   adsurl = {http://adsabs.harvard.edu/abs/2018arXiv180403288S},
  adsnote = {Provided by the SAO/NASA Astrophysics Data System}
}
```

### Software ###

The following table lists the various software packages, and their respective licenses, used to construct the virtual machine.

| Package | License | Version |
|---------|---------|---------|
|    Acl     |    GNU GPL     |    2.2.51     |
|    Linux-PAM     |    GNU GPL and BSD License     |    1.1.6     |
|    Python     |    PSF-2     |    2.7.6.1     |
|    XML-Parser     |    Artistic License 1.0 or GNU GPL     |    2.44     |
|    alsa-lib     |    GNU GPL     |    1.0.27.2     |
|    attr     |    GNU GPL     |    2.4.47     |
|    autoconf     |    GNU GPL     |    2.69     |
|    automake     |    GNU GPL     |    1.15.1     |
|    bash     |    GNU GPL     |    4.2     |
|    bc     |    GNU GPL     |    1.06.95     |
|    binutils     |    GNU GPL     |    2.25.1     |
|    bison     |    GNU GPL     |    3.0.2     |
|    bzip2     |    GNU GPL     |    1.0.6     |
|    cmake     |    BSD License     |    3.9.4     |
|    cmake_modules     |    BSD License     |    0.3     |
|    console_bridge     |    BSD License     |    0.4     |
|    coreutils     |    GNU GPL     |    8.21     |
|    curl     |    MIT     |    7.36.0     |
|    dbus     |    AFL-2.1, GPL-2     |    1.8.16     |
|    dbus-glib     |    AFL-2.1, GPL-2     |    0.100.2     |
|    diffutils     |    GNU GPL     |    3.3     |
|    e2fsprogs     |    GNU GPL     |    1.42.7     |
|    eigen     |    MPL-2     |    3.2.4     |
|    eudev     |    GNU GPL     |    2.1.1     |
|    expat     |    MIT License     |    2.1.0     |
|    ffmpeg     |    LGPL-2.1+     |    1.2.6     |
|    file     |    BSD-like License-5     |    5.17     |
|    findutils     |    GNU GPL     |    4.4.2     |
|    flac     |    BSD License and GNU GPL     |    1.3.0     |
|    flex     |    BSD     |    2.6.4     |
|    gawk     |    GNU GPL     |    4.1.1     |
|    gcc     |    GNU GPL     |    4.8.1     |
|    gcc     |    GNU GPL     |    5.4.0     |
|    gdb     |    GNU GPL     |    7.8.1     |
|    gdbm     |    GNU GPL     |    1.8.3     |
|    gettext     |    GNU GPL     |    0.18.3.2     |
|    glibc     |    GNU GPL     |    2.21     |
|    gmp     |    GNU GPL     |    5.1.3     |
|    gnutls     |    GNU LGPL     |    3.3.4     |
|    gperf     |    GNU GPL     |    3.1     |
|    grep     |    GNU GPL     |    2.16     |
|    groff     |    GNU GPL     |    1.22.3     |
|    grub     |    GNU GPL     |    2.02     |
|    gzip     |    GNU GPL     |    1.5     |
|    iana-etc     |    Open Software License     |    2.30     |
|    inetutils     |    GNU GPL     |    1.9.4     |
|    intltool     |    GNU GPL     |    0.51.0     |
|    iproute2     |    GNU GPL     |    4.12.0     |
|    json-c     |    MIT License     |    0.11     |
|    kbd     |    GNU GPL     |    2.0.4     |
|    kmod     |    GNU LGPL     |    17     |
|    less     |    Dual: GPL, BSD-like License     |    457     |
|    lfs-bootscripts     |    Creative Commons and MIT     |    20170626     |
|    libarchive     |    BSD License     |    3.1.2     |
|    libcap     |    GNU GPL or BSD-like license     |    2.22     |
|    libffi     |    MIT License     |    3.0.13     |
|    libjpeg-turbo     |    IJG, BSD, zlib license     |    1.3.1     |
|    libmadb     |    GNU GPL     |    0.15.1     |
|    libogg     |    BSD     |    1.3.1     |
|    libpipeline     |    GNU GPL     |    1.4.2     |
|    libpng     |    libpnglicense     |    1.6.10     |
|    libsamplerate     |    BSD License     |    0.1.7     |
|    libsndfile     |    GNU LGPL     |    1.0.25     |
|    libtasn1     |    GNU LGPL     |    2.14     |
|    libtheora     |    BSD License     |    1.1.1     |
|    libtool     |    GNU GPL     |    2.4.2     |
|    libusb     |    GNU LGPL     |    1.0.19     |
|    libvorbis     |    BSD     |    1.3.4     |
|    libxml2     |    MIT License     |    2.9.1     |
|    linux     |    GNU GPL     |    4.0.4     |
|    lz4     |    BSD License     |    1.0.6     |
|    m4     |    GNU GPL     |    1.4.18     |
|    make     |    GNU GPL     |    3.82     |
|    man-db     |    GNU GPL     |    2.7.6.1     |
|    man-pages     |    Multiple Licenses     |    4.12     |
|    mpc     |    GNU LGPL     |    1.0.3     |
|    mpfr     |    GNU LGPL and GNU GPL     |    3.1.2     |
|    nano     |    GNU GPL     |    2.8.7     |
|    nasm     |    BSD     |    2.11.05     |
|    ncurses     |    X11 License     |    5.9     |
|    nettle     |    GNU GPL and GNU LGPL     |    2.7.1     |
|    opencv     |    BSD     |    2.4.9     |
|    opus     |    BSD     |    1.0.2     |
|    patch     |    GNU GPL     |    2.7.1     |
|    pcre     |    BSD     |    8.33     |
|    perl     |    Artistic License 1.0 or GNU GPL     |    5.16.3     |
|    pip     |    Free Software     |    1.4.1     |
|    pkg-config     |    GNU GPL     |    0.29.2     |
|    procps-ng     |    GNU GPL and GNU LGPL     |    3.3.9     |
|    psmisc     |    GNU GPL     |    23.1     |
|    pulseaudio     |    GNU LGPL     |    6.0     |
|    readline     |    GNU GPL     |    6.2     |
|    rsync     |    GNU GPL     |    3.0.9     |
|    sbc     |    GNU GPL     |    1.2     |
|    sed     |    GNU GPL     |    4.2.1     |
|    setuptools     |    MIT     |    2.2     |
|    shadow     |    Artistic License or BSD-like License     |    4.2.1     |
|    speex     |    BSD     |    1.2     |
|    sqlite-autoconf     |    Public Domain     |    3081101     |
|    sudo     |    ISC Style     |    1.8.1     |
|    sysklogd     |    GNU GPL     |    1.5     |
|    sysvinitdsf     |    GNU GPL     |    2.88     |
|    tar     |    GNU-GPL     |    1.27.1     |
|    tcl     |    BSD-like License     |    8.6.7     |
|    texinfo     |    GNU GPL     |    6.4     |
|    tiff     |    BSD License     |    4.0.3     |
|    tinyxml     |    zlib license     |    1.0.1     |
|    tzdata     |    Public Domain or BSD     |         |
|    unzip     |    BSD License     |    6.0     |
|    util-linux     |    GNU GPL     |    2.24.1     |
|    vim80     |    Free Software (Vim License), charityware     |    8.0     |
|    which     |    GNU GPL     |    2.21     |
|    x264     |    GNU GPL     |    20140308     |
|    xz     |    GNU GPL and GNU LGPL     |    5.0.5     |
|    zip     |    BSD License     |    3.0     |
|    zlib     |    zlib license     |    1.2.8     |
